/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.yuweiguocn.library.greendao;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class LogUtils {

    public static void d(String tag, String msg) {
        HiLog.debug(getLabel(tag), msg);
    }

    public static void i(String tag, String msg) {
        HiLog.info(getLabel(tag), msg);
    }

    public static void w(String tag, String msg) {
        HiLog.warn(getLabel(tag), msg);
    }

    public static void e(String tag, String msg) {
        HiLog.error(getLabel(tag), msg);
    }

    private static HiLogLabel getLabel(String tag) {
        HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 001, tag);
        return LABEL;
    }
}
