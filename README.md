# GreenDaoUpgradeHelper

**本项目是基于开源项目 GreenDaoUpgradeHelper 进行ohos化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/yuweiguocn/GreenDaoUpgradeHelper ）追踪到原项目版本**

#### 项目介绍

- 项目名称：GreenDaoUpgradeHelper
- 所属系列：ohos的第三方组件适配移植
- 功能：数据库升级
- 项目移植状态：完成
- 调用差异：无差异。
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/yuweiguocn/GreenDaoUpgradeHelper
- 基线release版本：V0.1.0,SHA1:4348b1765aea6064787401e1d865a45979c55cb0
- 编程语言：Java
- 外部库依赖：无

#### 演示效果
![avatar](preview.gif)

#### 安装教程

##### 方案一：

1. 编译har包GreenDaoUpgradeHelper.har。

2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       ……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：

  1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     api 'io.openharmony.tpc.thirdlib:greendaocore:1.0.0'
     implementation 'com.github.yuweiguocn.library.greendao:GreenDaoUpgradeHelper:1.0.0'
 }
```


#### 使用说明：
有关该项目的业务实现，请参见“entry”文件夹.

##### 第一步: 添加一个扩展的类继承**DaoMaster。OpenHelper**，添加一个构造函数，实现**onUpgrade**方法和调用**migrate**方法，参数都是生成的Dao类:
```java
        public class MySQLiteOpenHelper extends DaoMaster.OpenHelper {
            public MySQLiteOpenHelper(Context context, String name, ResultSetHook factory) {
                super(context, name, factory);
            }
        
            @Override
            public void onUpgrade(Database db, int oldVersion, int newVersion) {
                MigrationHelper.migrate(db, new MigrationHelper.ReCreateAllTableListener() {
                    @Override
                    public void onCreateAllTables(Database db, boolean ifNotExists) {
                        DaoMaster.createAllTables(db, ifNotExists);
                    }
                    @Override
                    public void onDropAllTables(Database db, boolean ifExists) {
                        DaoMaster.dropAllTables(db, ifExists);
                    }
                },GroupDao.class, TestData2Dao.class, TestData3Dao.class);
            }
        }
```
##### 第二步: 初始化
```java
    ......

        //MigrationHelper.DEBUG = true;  //if you want see the log info,default is false
        MySQLiteOpenHelper helper = new MySQLiteOpenHelper(this, "test.db",
                null);
        daoMaster = new DaoMaster(helper.getWritableDatabase());

    ......
```

#### 版本迭代

- v1.0.0

1. 目前支持功能如下：
   - 数据库数据操作，数据库升级

#### 许可信息

    ```
    Copyright 2016 yuweiguocn
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    ```
