/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.yuweiguocn.demo.greendao;


import com.github.yuweiguocn.demo.greendao.about.AboutAbility;
import junit.framework.TestCase;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Text;
import org.junit.Assert;

public class UITest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        sleep(3);
    }


    public void testMainAbility() {
        Ability mainAbility = EventHelper.startAbility(MainAbility.class);
        DependentLayout title = (DependentLayout)mainAbility.findComponentById(ResourceTable.Id_title);
        int height = title.getHeight();
        Assert.assertEquals(height,56*3);
    }

    public void testAboutGithubAbility() {
        Ability mainAbility = EventHelper.startAbility(AboutAbility.class);
        Text github = (Text)mainAbility.findComponentById(ResourceTable.Id_github);
        String str = github.getText();
        Assert.assertEquals(str,"GitHub: http://github.com/yuweiguocn/");
    }

    public void testAboutWeiboAbility() {
        Ability mainAbility = EventHelper.startAbility(AboutAbility.class);
        Text weibo = (Text)mainAbility.findComponentById(ResourceTable.Id_weibo);
        String str = weibo.getText();
        Assert.assertEquals(str,"weibo: @于卫国");
    }


    private void sleep(int duration) {
        try {
            Thread.sleep(duration * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}