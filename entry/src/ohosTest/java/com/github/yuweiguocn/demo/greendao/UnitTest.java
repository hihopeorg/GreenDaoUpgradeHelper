/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.yuweiguocn.demo.greendao;


import com.github.yuweiguocn.demo.greendao.db.*;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import org.junit.Assert;

import java.util.Date;

public class UnitTest extends TestCase {
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;
    private DaoMaster daoMaster;
    private TestData2Dao testData2Dao;

    public void setUp() throws Exception {
        super.setUp();
        mContext = sAbilityDelegator.getAppContext();
        MySQLiteOpenHelper helper = new MySQLiteOpenHelper(mContext, "test.db",
                null);
        daoMaster = new DaoMaster(helper.getWritableDb());
    }

    public void tearDown() throws Exception {
    }

    public void testInsertData3() {
        TestData2 testData3 = new TestData2();
        testData2Dao = daoMaster.newSession().getTestData2Dao();
        testData2Dao.insert(testData3);
        Assert.assertEquals(testData2Dao.loadAll().contains(testData3), true);
    }

    public void testInsertGroup() {
        Group group1 = new Group();
        group1.setTestBoolean(true);
        group1.setTestDate(new Date());
        group1.setTestString(String.valueOf(System.currentTimeMillis()));
        group1.setTestLong(System.currentTimeMillis());
        GroupDao groupDao = daoMaster.newSession().getGroupDao();
        groupDao.insert(group1);
        Assert.assertEquals(groupDao.loadAll().contains(group1), true);
    }
}