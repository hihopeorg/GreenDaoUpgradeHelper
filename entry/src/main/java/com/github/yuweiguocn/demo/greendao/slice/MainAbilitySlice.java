/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.yuweiguocn.demo.greendao.slice;

import com.github.yuweiguocn.demo.greendao.ResourceTable;
import com.github.yuweiguocn.demo.greendao.db.*;
import com.github.yuweiguocn.library.greendao.LogUtils;
import com.github.yuweiguocn.library.greendao.MigrationHelper;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnSelectListener;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;

import java.util.Date;

public class MainAbilitySlice extends AbilitySlice {

    private DaoMaster daoMaster;
    private Intent mIntent;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        MigrationHelper.DEBUG = true;

        Image mAbout = (Image) findComponentById(ResourceTable.Id_menu);
        mAbout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showMultiPopup(component);
            }
        });

        MySQLiteOpenHelper helper = new MySQLiteOpenHelper(this, "test.db",
                null);
        daoMaster = new DaoMaster(helper.getWritableDb());
        TestData2 testData3 = new TestData2();
        TestData2Dao testData2Dao = daoMaster.newSession().getTestData2Dao();
        testData2Dao.insert(testData3);
        Group group1 = new Group();
        group1.setTestBoolean(true);
        group1.setTestDate(new Date());
        group1.setTestString(String.valueOf(System.currentTimeMillis()));
        group1.setTestLong(System.currentTimeMillis());
        GroupDao groupDao = daoMaster.newSession().getGroupDao();
        groupDao.insert(group1);

        LogUtils.d("MigrationHelper", "TestData2 " + testData2Dao.loadAll().toString());
        LogUtils.d("MigrationHelper", "Group " + groupDao.loadAll().toString());
    }

    private void showMultiPopup(Component component) {
        new XPopup.Builder(getContext())
                .hasShadowBg(false)
                .isDestroyOnDismiss(true)
                .atView(component)
                .isComponentMode(true, component)
                .asAttachList(new String[]{"About"},
                        new int[]{},
                        new OnSelectListener() {
                            @Override
                            public void onSelect(int position, String text) {
                                if (position == 0) {
                                    if(mIntent == null){
                                        mIntent = new Intent();
                                    }
                                    Operation operation = new Intent.OperationBuilder()
                                            .withDeviceId("")
                                            .withBundleName(MainAbilitySlice.this.getBundleName())
                                            .withAbilityName("com.github.yuweiguocn.demo.greendao.about.AboutAbility")
                                            .build();
                                    mIntent.setOperation(operation);
                                    startAbility(mIntent);
                                }
                            }
                        }).show();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
