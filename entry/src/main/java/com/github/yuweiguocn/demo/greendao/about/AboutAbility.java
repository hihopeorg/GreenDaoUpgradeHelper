package com.github.yuweiguocn.demo.greendao.about;


import com.github.yuweiguocn.demo.greendao.ResourceTable;
import com.github.yuweiguocn.demo.greendao.base.BaseActivity;
import com.github.yuweiguocn.demo.greendao.bean.About;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class AboutAbility extends BaseActivity implements AboutContract.View, Component.ClickedListener {

    private AboutClickHandlers aboutClickHandlers = new AboutClickHandlers();
    private AboutPresenter presenter;
    private Text mGithub,mWeibo,mGmail,mBlog,mJianshu;
    private Image mBack;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_about);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#303F9F"));
        initComponents();
        presenter = new AboutPresenter(this);
        presenter.loadData();
    }

    private void initComponents() {
        mGithub = (Text) findComponentById(ResourceTable.Id_github);
        mWeibo = (Text) findComponentById(ResourceTable.Id_weibo);
        mGmail = (Text) findComponentById(ResourceTable.Id_gmail);
        mBlog = (Text) findComponentById(ResourceTable.Id_blog);
        mJianshu = (Text) findComponentById(ResourceTable.Id_jianshu);
        mBack = (Image) findComponentById(ResourceTable.Id_back);

        mGithub.setClickedListener(this);
        mWeibo.setClickedListener(this);
        mGmail.setClickedListener(this);
        mBlog.setClickedListener(this);
        mJianshu.setClickedListener(this);
        mBack.setClickedListener(this);
    }


    @Override
    public void onSucess(About about) {
        mGithub.setText("GitHub: "+about.github);
        mWeibo.setText("weibo: "+about.weibo);
        mGmail.setText("gemail: "+about.email);
        mBlog.setText("blog: "+about.blog);
        mJianshu.setText("jianshu: "+about.jianshu);
    }

    @Override
    public void setPresenter(AboutContract.Presenter presenter) {

    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        switch (id){
            case ResourceTable.Id_github:
                aboutClickHandlers.onClickBlog(this);
                break;
            case ResourceTable.Id_weibo:
                aboutClickHandlers.onClickWeibo(this);
                break;
            case ResourceTable.Id_gmail:
                break;
            case ResourceTable.Id_blog:
                aboutClickHandlers.onClickBlog(this);
                break;
            case ResourceTable.Id_jianshu:
                aboutClickHandlers.onClickJianshu(this);
                break;
            case ResourceTable.Id_back:
                terminateAbility();
                break;
        }
    }
}
