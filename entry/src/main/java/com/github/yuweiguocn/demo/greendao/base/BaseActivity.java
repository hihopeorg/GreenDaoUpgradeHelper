package com.github.yuweiguocn.demo.greendao.base;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;

/**
 * Created by growth on 7/9/16.
 */
public class BaseActivity extends Ability {

    protected Context ct;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ct = this;
    }
}
