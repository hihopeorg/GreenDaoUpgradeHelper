package com.github.yuweiguocn.demo.greendao.about;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.utils.net.Uri;

/**
 * Created by growth on 7/9/16.
 */
public class AboutClickHandlers {

    public void onClickWeibo(Ability ability) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse("http://weibo.com/weiguo58");
        intent.setUri(uri);
        ability.startAbility(intent);
    }

    public void onClickGitHub(Ability ability) {

        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse("http://github.com/yuweiguocn/");
        intent.setUri(uri);
        ability.startAbility(intent);
    }

    public void onClickBlog(Ability ability) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse("http://yuweiguocn.github.io/");
        intent.setUri(uri);
        ability.startAbility(intent);
    }

    public void onClickJianshu(Ability ability) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse("http://www.jianshu.com/users/4cb0c84627c4");
        intent.setUri(uri);
        ability.startAbility(intent);
    }



}
